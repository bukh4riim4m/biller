<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detailsaldos extends Model
{
  protected $fillable = [
      'id','iduser', 'bank','nominal','status','created_at'
  ];
  protected $hidden = [
      'updated_at',
  ];
}
