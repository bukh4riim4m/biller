<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
  protected $fillable = [
      'userid', 'transactionid','hp_id_pel','paket','harga','status','sn','saldo','note','nta','created_at'
  ];
  protected $hidden = [
      'updated_at',
  ];
}
