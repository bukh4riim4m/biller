<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlnPascaController extends Controller
{
    public function plnpascabayar(Request $request)
    {
        if (date('H') > 23 || date('H') < 1) {
            $respon = [
              'code'=>'400',
              'message'=>'Saat ini sedang maintenance jam 23:00 s/d 01:00 WIB'
            ];
            return $respon;
        }

        function curlPost($url, $data, $header)
        {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array($header));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_TIMEOUT, 90);
            $result = curl_exec($ch);
            curl_close($ch);
            return ($result);
        }

        $url = 'http://180.250.247.3:6856/apis/';
        $tid = $request['trxid'];
        $adm = '2500';
        $pin ='2129';
        $cmd = 'CEKPLNPSC';
        $usr = 'MTR1648';
        $mdn = $request['meteran'];
        $sig = md5($tid.$pin);
        $datas = array('cmd'=>$cmd,'tid'=>$tid,'uid'=>$usr, 'adm'=>$adm, 'mdn'=>$mdn,'sig'=>$sig);
        $data = json_encode($datas);
        $header = "Content-Type: application/json";
        $result = curlPost($url, $data, $header);
        return $result;
        if ($result) {
            // return 'OK';
            $resarr = json_decode($result, true);
            if ($resarr['rescode'] == '0000') {
                $dataa = explode("|", $resarr['info']);
                $respon = [
                  'result'=>'success',
                  'meteran'=>$dataa[2],
                  'tagihan'=>$dataa[1],
                  'atas_nama'=>$dataa[4],
                  'periode'=>$dataa[10],
                ];
                return response()->json($respon);
            } else {
                $respon = [
                'result'=>'error',
                'message'=>'Pengecekan Gagal'
              ];
                return response()->json($respon);
            }
        } else {
            $respon = [
            'result'=>'error',
            'message'=>'Tidak dapat menampilkan data'
          ];
            return response()->json($respon);
        }
    }
}
