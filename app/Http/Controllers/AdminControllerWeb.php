<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Detailsaldos;
use App\User;
use App\Transactions;
use App\Datadepositagen;
use App\Bukusaldoagen;
use App\Products;
use App\Apis;
use App\Agen;
use App\Transaksiagen;
use Auth;
use DB;
use Log;

class AdminControllerWeb extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function condef()
    {
        $detailsaldo = Detailsaldos::orderBy('id', 'DESC')->get();
        return view('admin.konfirmasi_deposit', compact('detailsaldo'));
    }
    public function datadeposit()
    {
        $detailsaldo = Detailsaldos:: orderBy('id', 'DESC')->get();
        return view('admin.data_deposit', compact('detailsaldo'));
    }

    public function prosdep($id)
    {
        $detailsaldo = Detailsaldos::find($id);
        if ($detailsaldo->status == 'Pending') {
            $userid = User::find($detailsaldo->iduser);

            $userid->saldo = $userid->saldo + $detailsaldo->nominal;
            $detailsaldo->status = 'Sukses';
            $detailsaldo->update();
            $userid->update();
            $transaction = Transactions::create([
        'userid'=>$userid->id,
        'transactionid'=>12345,
        'hp_id_pel'=>$userid->id,
        'paket'=>'TAMBAH SALDO',
        'harga'=>$detailsaldo->nominal,
        'nta'=>0,
        'status'=>'Sukses',
        'sn'=>'SALDO BERTAMBAH   '.$userid->id,
        'saldo'=>$userid->saldo
      ]);
        }
        return Redirect("/condef");
    }

    public function canceldep($id)
    {
        $detailsaldo = Detailsaldos::find($id);
        if ($detailsaldo->status == 'Pending') {
            $detailsaldo->status = 'Cancel';
            $detailsaldo->update();
        }
        return Redirect("/condef");
    }

    public function datpul()
    {
        $transaction = Transactions::orderBy('id', 'ASC')->get();
        return view('admin.datpul', compact('transaction'));
    }

    public function dathar()
    {
        $products = Products::orderBy('id', 'ASC')->get();
        return view('admin.data_harga', compact('products'));
    }

    public function updateuntung(Request $request)
    {
        $products = Products::find($request->id);
        $products->untung = $request->price;
        $products->update();
        return Redirect('/dathar');
    }
    public function updateharga($id)
    {

    // $request->user()->id;
        $show = Apis::find(1);

        $url = 'https://javah2h.com/api/connect/';

        $header = array(
    'h2h-userid:'. $show->userid,
    'h2h-key: '.$show->key, // lihat hasil autogenerate di member area
    'h2h-secret:'. $show->secret, // lihat hasil autogenerate di member area
    );

        // echo json_encode($header);
        $data = array(
        'inquiry' => 'HARGA', // konstan
        'code' => $id, // pilihan: pln, pulsa, game
        );


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);
        $json = json_decode($result, true);
        $hasil = $json['message'];
        // return json_encode($hasil,201);
        foreach ($hasil as $value) {
            if ($produks = Products::where('code', $value['code'])->first()) {
                $produks->price = $value['price'];
                $produks->status = $value['status'];
                $produks->update();
            }
            // $produks = Products::create([
      //   'code'=>$value['code'],
      //   'description'=>$value['description'],
      //   'operator'=>$value['operator'],
      //   'price'=>$value['price'],
      //   'status'=>$value['status'],
      //   'provider_sub'=>$value['provider_sub']
      // ]);
        }
        return Redirect('/dathar');
    }
    public function datharagent()
    {
        $products = Agen::orderBy('id', 'ASC')->get();
        return view('admin.agen.harga_agen', compact('products'));
    }
    public function updateagent($id)
    {

    // $request->user()->id;
        $show = Apis::find(1);

        $url = 'https://javah2h.com/api/connect/';

        $header = array(
    'h2h-userid:'. $show->userid,
    'h2h-key: '.$show->key, // lihat hasil autogenerate di member area
    'h2h-secret:'. $show->secret, // lihat hasil autogenerate di member area
    );

        // echo json_encode($header);
        $data = array(
    'inquiry' => 'HARGA', // konstan
    'code' => $id, // pilihan: pln, pulsa, game
    );


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);
        $json = json_decode($result, true);
        $hasil = $json['message'];
        // return json_encode($hasil,201);
        foreach ($hasil as $value) {
            if ($produks = Agen::where('code', $value['code'])->first()) {
                $produks->price = $value['price']+300;
                $produks->untung = 300;
                $produks->status = $value['status'];
                $produks->update();
            }
            // $produks = Agen::create([
      //   'code'=>$value['code'],
      //   'description'=>$value['description'],
      //   'operator'=>$value['operator'],
      //   'price'=>$value['price'],
      //   'status'=>$value['status'],
      //   'provider_sub'=>$value['provider_sub']
      // ]);
        }
        return Redirect('/agent');
    }

    public function user()
    {
        $users = User::orderBy('saldo', 'DESC')->get();
        return view('admin.data_user', compact('users'));
    }
    public function depositagen(Request $request)
    {
        $detailsaldo = Datadepositagen::where('aktif', 1)->get();
        return view('admin.agen.deposit_agen', compact('detailsaldo'));
    }
    public function bukusaldo(Request $request){
      if ($request->id_agen) {
        $from = $request->tahun.'-'.$request->bulan.'-1';
        $until = $request->tahun.'-'.$request->bulan.'-31';
        $dari = date($from);
        $sampai = date($until);
        $users = User::find($request->id_agen);
        $datas = Bukusaldoagen::whereBetween('tgl_trx', [$dari,$sampai])->where('user_id',$users->id)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        return view('admin.agen.bukusaldo', compact('users','datas'));
      }
      $datas=[];
      return view('admin.agen.bukusaldo', compact('users','datas'));
    }
    public function prosesdepagen($id)
    {
        $datas = Datadepositagen::find($id);
        if ($datas->status =='Menunggu') {
            DB::beginTransaction();
            try {
                $update = Datadepositagen::find($id);
                $update->status = 'Berhasil';
                $saldo = User::find($update->user_id);
                $total = $saldo->saldo + $update->transfer;
                $saldo->saldo = $total;
                Bukusaldoagen::create([
          'user_id'=>$update->user_id,
          'no_trx'=>date('Ymdhi').rand(10, 100),
          'tgl_trx'=>date('Y-m-d'),
          'nominal'=>$update->transfer,
          'mutasi'=>'Debet',
          'saldo'=>$total,
          'aktif'=>1,
          'keterangan'=>'Topup Saldo',
          'created_by'=>Auth::user()->id
        ]);
                $update->update();
                $saldo->update();
            } catch (\Exception $e) {
                Log::info('Gagal Proses DEP:'.$e->getMessage());
                DB::rollback();
                flash()->overlay('Gagal Proses DE.', 'INFO');
                return redirect()->back();
            }
            DB::commit();
            flash()->overlay('Transaksi DEP Berhasil, Mohon di tunggu.', 'INFO');
            return redirect()->back();
        }
    }
    public function cancelagen($id)
    {
        $datas = Datadepositagen::find($id);
        if ($datas->status =="Menunggu") {
            DB::beginTransaction();
            try {
                $data = Datadepositagen::find($id);
                $data->status = 'Batal';
                $data->update();
            } catch (\Exception $e) {
                Log::info('Gagal Proses DEP:'.$e->getMessage());
                DB::rollback();
                flash()->overlay('Gagal di Cancel', 'INFO');
                return redirect()->back();
            }
            DB::commit();
            flash()->overlay('Berhasil di Cancel', 'INFO');
            return redirect()->back();
        }
    }
    public function transaksiagent(Request $request)
    {
        $datas = Transaksiagen::orderBy('id', 'ASC')->get();
        return view('admin.agen.dataTransaksi', compact('datas'));
    }
}
