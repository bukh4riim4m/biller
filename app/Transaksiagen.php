<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksiagen extends Model
{
  protected $fillable = [
      'user_id','inquiry', 'trxid_api','hp_id_pel','paket','callback','harga','nta','status','sn','saldo','note','created_at','updated_at'
  ];
}
