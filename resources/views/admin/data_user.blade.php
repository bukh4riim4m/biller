@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="table-responsive">
                    <table  class="table table-striped custom-table" width="100%">
                      <thead>
                        <tr>
                          <td width="5" align="center"><strong>No</strong></td>
                          <td align="center"><strong>Tanggal</strong></td>
                          <td align="center"><strong>ID User</strong></td>
                          <td align="center"><strong>Nama</strong></td>
                          <td align="center"><strong>No Hp</strong></td>
                          <td align="center"><strong>Saldo</strong></td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $id=0; ?>
                        @foreach($users as $key)
                        <?php $id+=1; ?>
                        <tr>
                          <td>{{$id}}</td>
                          <td align="center">{{ $key->created_at}}</td>
                          <td align="center">ID-{{ $key->id}}</td>
                          <td align="center">{{ $key->name}}</td>
                          <td align="center">{{ $key->hp }}</td>
                          <td align="right">{{ number_format($key->saldo)}}</td>
                        </tr>
                        @endforeach
                      </tbody>


                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
