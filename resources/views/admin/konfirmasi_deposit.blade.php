@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">KONFIRMASI DEPOSIT</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="table-responsive">
                    <table border="0" class="table table-striped custom-table">
                      <tr>
                        <td width="5" align="center"><strong>No</strong></td>
                        <td align="center"><strong>Tangal</strong></td>
                        <td align="center"><strong>ID User</strong></td>
                        <td align="center"><strong>Bank</strong></td>
                        <td align="center"><strong>Status</strong></td>
                        <td align="center"><strong>Nominal</strong></td>
                        <td width="180"><strong>action</strong></td>
                      </tr>
                      <?php $id=0; ?>
                      @foreach($detailsaldo as $key)
                      <?php $id+=1; ?>
                      <tr>
                        <td>{{$id}}</td>
                        <td align="center">{{ $key->created_at}}</td>
                        <td align="center">{{ $key->iduser}}</td>
                        <td align="center">{{ $key->bank}}</td>
                        <td align="center">{{ $key->status}}</td>
                        <td align="right">{{ number_format($key->nominal)}}</td>
                        @if($key->status =='Pending')
                        <td align="center"><a onclick="confirm('Proses ?')" href="{{ url('/prosdep'.$key->id) }}" class="btn-primary btn">Proses</a><a onclick="confirm('Batalkan ?')" href="{{ url('/canceldep'.$key->id) }}" class="btn-danger btn">Cancel</a></td>
                        @else
                        <td align="center"></td>
                        @endif
                      </tr>
                      @endforeach
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
