<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v1'], function () {
    Route::post('callback', 'API\ResponseController@callback');
    Route::post('agent', 'API\CallbackController@agent');
    Route::post('user/signin', 'API\PassportController@login');
    Route::post('user/register', 'API\PassportController@register');
    Route::get('12345/6789', 'API\AuthController@cek');
    Route::get('listvoucherlistrik', 'API\CekController@listvoucherlistrik');
    Route::get('datatransaksi{id}', 'API\CekController@datatransaksi');
    Route::get('datasaldo{id}', 'API\CekController@datasaldo');
    Route::get('hargapulsa/{id}', 'API\CekController@hargapulsa');



    Route::get('provider', 'API\CekController@provider');

    Route::post('testback', 'API\ResponseController@testback');
});
Route::group(['prefix' => 'v1','middleware'=>'auth:api'], function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::post('user/logout', 'API\PassportController@logout');
    Route::post('get-details', 'API\PassportController@getDetails');
    Route::post('belipaket', 'API\TransactionController@belipaket');
    Route::post('prosespulsa', 'API\ProsesController@prosespulsa');
    Route::post('isisaldo', 'API\ProsesController@tambahsaldo');
    Route::post('updateproduct', 'API\ProsesController@updateproduct');
    Route::post('editpass', 'API\ProsesController@editpass');
    //CekController
    Route::post('prov_token', 'API\CekController@prov_token');
    Route::post('cekharga', 'API\CekController@cekharga');
    Route::post('detailPln', 'API\CekController@detailPln');
    Route::post('hargavoucherlistrik', 'API\CekController@hargavoucherlistrik');
    Route::post('cekstatus', 'API\CekController@cekStatus');
    //Laundry
    Route::post('paketcuci', 'API\PassportController@paketcuci');
    //ROUTE AGEN
    Route::post('proses-agent', 'API\SuplayerController@prosesagent');
    Route::post('topup-suplayer', 'API\SuplayerController@suplayer');
    Route::post('data-suplayer', 'API\SuplayerController@datasuplayer');
    Route::post('buku-saldo-suplayer', 'API\SuplayerController@bukusaldosuplayer');
    Route::post('cek-status', 'API\SuplayerController@cek_status_agent');
    Route::post('cek-pln-pascabayar', 'API\PlnPascaController@plnpascabayar');
    Route::get('hargaagent/{id}', 'API\SuplayerController@hargaagent');
});
